#!/bin/sh

# By default nixpkgs will install the nixpkgs master channel (aka non NixOS installations)
# If using NixOS change this to the approriate channel (e.g. "release-24.05")
nixpkgs_release="master"

# Add nix-defexpr/channels nix path or else current shell won't find home-manager command
export NIX_PATH=$NIX_PATH:$HOME/.nix-defexpr/channels

# Install home-manager
nix-channel --add https://github.com/nix-community/home-manager/archive/${nixpkgs_release}.tar.gz home-manager
nix-channel --update
nix-shell '<home-manager>' -A install
