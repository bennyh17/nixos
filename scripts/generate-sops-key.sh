#!/bin/sh

# Ensure dir is present
mkdir -p ~/.config/sops/age

# Generate sops key
nix shell --extra-experimental-features nix-command --extra-experimental-features flakes \
  nixpkgs#age -c age-keygen -o ~/.config/sops/age/keys.txt
