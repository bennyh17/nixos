#!/bin/sh


# Default flake variables. Adjust these as needed
# Forward slashes must be escaped with '\' (e.g. '/dev/sda' must be escaped like '\/dev\/sda')
HOSTNAME="nixos"
SYSTEM="x86_64-linux"
PROFILE="default"
BOOTLOADER="grub"
PLATFORM="qemu"
BOOT_DEVICE="\/dev\/sda1"
ROOT_DEVICE="\/dev\/sda2"
CONTAINER_ENGINE="none";
TIMEZONE="Australia\/Darwin"
DESKTOP="disabled"
SLAVE_INTERFACE="eno1"
HYPERVISOR_DOMAIN="localhost"
BRIDGE_IP="192.168.1.2"
BRIDGE_PREFIX=24
BRIDGE_GATEWAY="192.168.1.1"
USERNAME="benny"
KUBE_INITIAL_MASTER_IP="192.168.1.3"


# Declare flake file relative to where the script is executed from
FLAKE_FILE="$(dirname $0)/../flake.nix"

# Make substitutions in flake file
sed -i "s/<hostname>/${HOSTNAME}/g" $FLAKE_FILE
echo "Setting hostname to ${HOSTNAME}"

sed -i "s/<system>/${SYSTEM}/g" $FLAKE_FILE
echo "Setting system to ${SYSTEM}"

sed -i "s/<profile>/${PROFILE}/g" $FLAKE_FILE
echo "Setting profile to ${PROFILE}"

sed -i "s/<bootloader>/${BOOTLOADER}/g" $FLAKE_FILE
echo "Setting bootloader to ${BOOTLOADER}"

sed -i "s/<platform>/${PLATFORM}/g" $FLAKE_FILE
echo "Setting platform to ${PLATFORM}"

sed -i "s/<boot_device>/${BOOT_DEVICE}/g" $FLAKE_FILE
echo "Setting boot_device to ${BOOT_DEVICE}"

sed -i "s/<root_device>/${ROOT_DEVICE}/g" $FLAKE_FILE
echo "Setting root_device to ${ROOT_DEVICE}"

sed -i "s/<container_engine>/${CONTAINER_ENGINE}/g" $FLAKE_FILE
echo "Setting container_engine to ${CONTAINER_ENGINE}"

sed -i "s/<timezone>/${TIMEZONE}/g" $FLAKE_FILE
echo "Setting timezone to ${TIMEZONE}"

sed -i "s/<desktop>/${DESKTOP}/g" $FLAKE_FILE
echo "Setting desktop to ${DESKTOP}"

sed -i "s/<slave_interface>/${SLAVE_INTERFACE}/g" $FLAKE_FILE
echo "Setting slave_interface to ${SLAVE_INTERFACE}"

sed -i "s/<hypervisor_domain>/${HYPERVISOR_DOMAIN}/g" $FLAKE_FILE
echo "Setting hypervisor_domain to ${HYPERVISOR_DOMAIN}"

sed -i "s/<bridge_ip>/${BRIDGE_IP}/g" $FLAKE_FILE
echo "Setting bridge_ip to ${BRIDGE_IP}"

sed -i "s/<bridge_prefix>/${BRIDGE_PREFIX}/g" $FLAKE_FILE
echo "Setting bridge_prefix to ${BRIDGE_PREFIX}"

sed -i "s/<bridge_gateway>/${BRIDGE_GATEWAY}/g" $FLAKE_FILE
echo "Setting bridge_gateway to ${BRIDGE_GATEWAY}"

sed -i "s/<username>/${USERNAME}/g" $FLAKE_FILE
echo "Setting username to ${USERNAME}"

sed -i "s/<kube_initial_master_ip>/${KUBE_INITIAL_MASTER_IP}/g" $FLAKE_FILE
echo "Setting kube_initial_master_ip to ${KUBE_INITIAL_MASTER_IP}"

echo
echo "Done!"
