{
  description = "Flake config for bennyh17";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-24.05";
    home-manager = {
      url = "github:nix-community/home-manager/release-24.05";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nixvim = {
      url = "github:nix-community/nixvim/nixos-24.05";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    sops-nix = {
      url = "github:Mic92/sops-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    hyprland = {
      # See https://github.com/hyprwm/Hyprland/issues/5891
      type = "git";
      url = "https://github.com/hyprwm/Hyprland";
      submodules = true;
    };
  };

  outputs = { self, nixpkgs, home-manager, sops-nix, nixvim, ... }@inputs:
  let
    pkgs = nixpkgs.legacyPackages.${system}; # Configure pkgs
    lib = nixpkgs.lib; # Configure lib

    # ---- SYSTEM SETTINGS ---- #
    hostname = "<hostname>";  # Default: "nixos"
    system = "<system>";  # Default: "x86_64-linux"
    bootloader = "<bootloader>";  # Default: "grub"
    timezone = "<timezone>";  # Default: "Australia/Darwin"
    profile = "<profile>";  # Default: "default"
    platform = "<platform>";  # Default: "qemu"
    boot_device= "<boot_device>";  # Default: "/dev/sda1"
    root_device= "<root_device>";  # Default: "/dev/sda2"
    container_engine = "<container_engine>";  # Default: "none"

    # ---- USER SETTINGS ---- #
    username = "<username>";  # Default: "benny"
    desktop = "<desktop>";  # Default: "disabled"

    # ---- HYPERVISOR SETTINGS ---- #
    slave_interface = "<slave_interface>";  # Default: "eno1"
    hypervisor_domain = "<hypervisor_domain>";  # Default "localhost"
    bridge_ip = "<bridge_ip>";  # Default: "192.168.1.2"
    bridge_prefix = <bridge_prefix>;  # Default: 24
    bridge_gateway = "<bridge_gateway>";  # Default: "192.168.1.1"

    # ---- K3S SETTINGS ---- #
    kube_initial_master_ip = "<kube_initial_master_ip>";  # Default: "192.168.1.3"
  in {

    nixosConfigurations = {

      base = lib.nixosSystem {
        inherit system;
        modules = [
          (./. + "/profiles"+("/"+profile)+"/configuration.nix")
        ];
        specialArgs = {
	  inherit inputs;
          inherit hostname;
          inherit system;
          inherit profile;
          inherit bootloader;
	  inherit platform;
	  inherit boot_device;
	  inherit root_device;
	  inherit container_engine;
          inherit timezone;
	  inherit desktop;
	  inherit slave_interface;
	  inherit hypervisor_domain;
	  inherit bridge_ip;
	  inherit bridge_prefix;
	  inherit bridge_gateway;
          inherit username;
          inherit kube_initial_master_ip;
        };
      };
    };

    homeConfigurations = {

      base = home-manager.lib.homeManagerConfiguration {
        inherit pkgs;
        modules = [ 
          (./. + "/profiles"+("/"+profile)+"/home.nix")
	  nixvim.homeManagerModules.nixvim
        ];
        extraSpecialArgs = {
          inherit username;
          inherit profile;
	  inherit desktop;
        };
      };

      shell-ninja = home-manager.lib.homeManagerConfiguration {
        inherit pkgs;
        modules = [ 
          ./modules/user/ninja-config.nix
	  nixvim.homeManagerModules.nixvim
        ];
        extraSpecialArgs = {
          inherit username;
        };
      };
    };
  };
}
