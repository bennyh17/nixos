# NixOS Infrastructure

## the flake.nix file is coded to be an entrypoint for a tool like ansible because I haven't yet figured out an elegant solution using nix. This is in no way the right way to do this but makes managing a fleet of nixos machines easier

### Directory Layout:
    - modules/   <-- Where shared .nix file configurations are kept (not actual Nix modules)
    - profiles/  <-- Different profiles for different hosts

modules/ - Contains my personal nix sub-configurations that are utilized by different 'profiles'.

profiles/ - Contains profiles which enables additional configuration either declared directly in their respective configuration.nix files or through importing modules. Each profile utilizies a default base-config.nix file which installs helpful software and sets up useful services with sensible defaults.
