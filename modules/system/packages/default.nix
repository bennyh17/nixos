{ pkgs, ... }:

{
  # System packages to install by default
  environment.systemPackages = with pkgs; [
    age
    asciiquarium
    bat
    dig
    file
    gcc
    git
    htop
    jq
    lshw
    ncdu
    python3
    sops
    sshpass
    speedtest-cli
    traceroute
    tree
    tldr
    unzip
    vim
    wget
  ];
}
