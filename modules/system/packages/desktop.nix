{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    chromium
    firefox
    libreoffice
  ];
}
