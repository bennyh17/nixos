{ lib, root_device, boot_device, ... }:

{
  imports = [ ];

  virtualisation.virtualbox.guest = {
    enable = true;
    draganddrop = true;
  };

  boot = {
    initrd = {
      availableKernelModules = [ "ata_piix" "ohci_pci" "ehci_pci" "ahci" "sd_mod" ];
      kernelModules = [ ];
    };
    kernelModules = [ ];
    extraModulePackages = [ ];
  };

  fileSystems."/" = {
    device = "${root_device}";
    fsType = "ext4";
  };

  fileSystems."/boot" = {
    device = "${boot_device}";
    fsType = "vfat";
    options = [ "fmask=0022" "dmask=0022" ];
  };

  networking.useDHCP = lib.mkDefault true;

  nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";
}
