{ pkgs, hostname, ... }:

{
  # Enable k3s server settings
  services.k3s = {
    enable = true;
    tokenFile = /tmp/kubetoken;
  };

  # Have to set kubeconfig variable ourseleves
  environment.sessionVariables = rec {
    KUBECONFIG = "$HOME/.kube/config";
  };

  # Open TCP ports in firewall for k3s
  networking.firewall.allowedTCPPorts = [ 
    6443
    2379
    2380
    7946 # For MetalLB
  ];

  # Open UDP ports in firewall for k3s
  networking.firewall.allowedUDPPorts = [
    8472
    7946 # For MetalLB
  ];

  # See https://github.com/longhorn/longhorn/issues/2166 for context
  systemd.tmpfiles.rules = [
    "L+ /usr/local/bin - - - - /run/current-system/sw/bin/"
  ];

  # Enable iscisd daemon for longhorn volumes
  services.openiscsi = {
    enable = true;
    name = "${hostname}";
  };

  # For longhorn
  environment.systemPackages = [ pkgs.nfs-utils ];
}
