{ pkgs, ... }:
let
  fallout = pkgs.fetchFromGitHub {
    owner = "shvchk";
    repo = "fallout-grub-theme";
    rev = "e8433860b11abb08720d7c32f5b9a2a534011bca";
    sha256 = "sha256-mvb44mFVToZ11V09fTeEQRplabswQhqnkYHH/057wLE=";
  };
in
{
  boot.loader.grub = {
    efiSupport = true;
    efiInstallAsRemovable = true;
    devices = [ "nodev" ];
    theme = fallout;
  };
}
