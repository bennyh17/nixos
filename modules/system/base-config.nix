{
  hostname,
  platform,
  bootloader,
  timezone,
  desktop,
  container_engine,
  ...
}:

{
  imports = [
    ./hardware-config/${platform}/hardware-configuration.nix
    ./bootloader/${bootloader}.nix
    ./desktop/${desktop}.nix
    ./containers/${container_engine}.nix
    ./users/default.nix
    ./packages/default.nix
    ./security/sshd.nix
    ./security/sops.nix
  ];

  # Set Locale
  i18n.defaultLocale = "en_AU.UTF-8";
  i18n.extraLocaleSettings = {
    LC_ADDRESS = "en_AU.UTF-8";
    LC_IDENTIFICATION = "en_AU.UTF-8";
    LC_MEASUREMENT = "en_AU.UTF-8";
    LC_MONETARY = "en_AU.UTF-8";
    LC_NAME = "en_AU.UTF-8";
    LC_NUMERIC = "en_AU.UTF-8";
    LC_PAPER = "en_AU.UTF-8";
    LC_TELEPHONE = "en_AU.UTF-8";
    LC_TIME = "en_AU.UTF-8";
  };

  nixpkgs.config.allowUnfree = true; # Allow unfree packages

  nix.settings.experimental-features = [ "nix-command" "flakes" ]; # Enable flakes

  networking.hostName = hostname; # Set the hostname

  time.timeZone = timezone; # Set the timezone

  system.stateVersion = "23.11"; # Version of NixOS which was initially installed
}
