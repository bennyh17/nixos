{ lib, username, ... }:

{
  # Force ssh to allow password authentication for bastion host
  services.openssh.settings.PasswordAuthentication = lib.mkOverride 50 true;

  # SSH keys for bastion host
  users.users.${username}.openssh.authorizedKeys.keys = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGVymV+gAXZpe8YFiWQ76LG6jtkYD9wsdexpyWgQdz+n work"
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDWh9gq+6RMT87VatRlPM7yNxK2Z3KyMfjzyizaOy010 benny@MC-HV-01"
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJ6kd3FGJsBK8mm1+dEpRf1vyYEQo5yzvWrsmMxnO/X1 benny@MC-DT-01"
  ];
}
