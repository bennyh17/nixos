{ inputs, username, ... }:

{
  imports = [ inputs.sops-nix.nixosModules.sops ];

  sops = {
    defaultSopsFile = ../../../secrets/secrets.yml;
    defaultSopsFormat = "yaml";
    age.keyFile = /home/${username}/.config/sops/age/keys.txt;
  };
} 
