{ config, pkgs, username, ... }:

{
  # Enable incoming ssh
  services.openssh = {
    enable = true;
    openFirewall = true;
    settings = {
      PasswordAuthentication = false;
      PermitRootLogin = "no";
    };
  };

  # SSH keys (only for default user)
  users.users.${username}.openssh.authorizedKeys.keys = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOzlYmoWjZYFeCNdMBCHBXmqpzK1IBmRiB3hNlsgEtre benny@MC-BH-01"
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILqUb3DvYnRpVHlz0KvMgf8zNnaPlmVqbj8c6CBFHnfL benny@MC-MN-01"
  ];

  # Required for oh-my-tmux ssh sessions to work correctly
  programs.ssh.extraConfig = "SetEnv TERM=screen-256color";

  # Enable fail2ban
  # services.fail2ban.enable = true; # Temporarily disabling as it it's properly configured as is
}
