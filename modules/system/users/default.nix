{ pkgs, username, ... }:

{
  # Upgrade shell from bash to zsh
  programs.zsh.enable = true;

  # Default user
  users.users.${username} = {
    isNormalUser = true;
    initialPassword = "changeme";
    shell = pkgs.zsh;
    extraGroups = [ "wheel" ];
  };
}
