{ pkgs, username, ... }:

{
  virtualisation.docker = {
    enable = true;
  };

  boot.kernel.sysctl = {
    "net.ipv4.ip_unprivileged_port_start" = 80;
  };

  environment.systemPackages = [
    pkgs.arion
    pkgs.docker-compose
  ];

  users.users.${username}.extraGroups = [ 
    "docker"
  ];
}
