{ ... }:

{
  imports = [
    ../packages/desktop.nix
    ./sddm.nix
    ./fonts.nix
  ];

  networking.networkmanager.enable = true;

  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa = {
      enable = true;
      support32Bit = true;
    };
    pulse.enable = true;
  };

  services.xserver = { 
    enable = true;
    layout = "au";
    xkbVariant = "";
    desktopManager.gnome.enable = true;
  };
}
