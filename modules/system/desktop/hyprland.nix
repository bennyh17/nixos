{ pkgs, inputs, ... }:

{
  imports = [
    ../packages/desktop.nix
    ./sddm.nix
    ./fonts.nix
  ];

  # Enable hyprland and set the package to the Hyprland nix package
  programs.hyprland = {
    enable = true;
    xwayland.enable = true;
    package = inputs.hyprland.packages."${pkgs.system}".hyprland;
  };

  environment.sessionVariables = {
    # To prevent cursor from sometimes becoming invisible
    WLR_NO_HARDWARE_CURSORS = "1";

    # Hint to electron apps to use Wayland
    NIXOS_OZONE_WL = "1";
  };

  hardware = {
    opengl.enable = true;

    # Required for most Wayland compositors 
    nvidia.modesetting.enable = true;
  };

  environment.systemPackages = with pkgs; [
    dunst
    networkmanagerapplet
    waybar
    swww
    rofi-wayland
  ];

  xdg.portal = {
    enable = true;
    extraPortals = [
      pkgs.xdg-desktop-portal-gtk
    ];
  };

  sound.enable = true;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa = {
      enable = true;
      support32Bit = true;
    };
    pulse.enable = true;
    jack.enable = true;
  };
}
