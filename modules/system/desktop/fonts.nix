{ pkgs, ... }:

{
  fonts = {
    enableDefaultFonts = true;
    packages = with pkgs; [
      (nerdfonts.override { fonts = [ "Terminus" ]; })
      noto-fonts-emoji
    ];
  };
}
