{ pkgs, ... }:

{
  # Source: https://discourse.nixos.org/t/how-install-python-packages-globally-with-home-manager/37025
  home.packages = [
    (pkgs.python311.withPackages (ppkgs: [
      ppkgs.pyyaml # Needed for controlling helm via ansible
      ppkgs.kubernetes # Needed for creating custom resources using ansible
    ]))
  ];
}
