{ ... }:

{
  programs.waybar = {
    enable = true;
    settings = {
      mainBar = {
        position = "top";
        modules-center = [
          "clock#1"
          "clock#2"
          "clock#3"
        ];
        "clock#1" = {
          format = "{:%a}";
        };
        "clock#2" = {
          format = "{:%H:%M}";
        };
        "clock#3" = {
          format = "{:%d-%m}";
        };
      };
    };
  };
}
