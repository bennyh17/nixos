{ pkgs, ... }:

let
  startupScript = pkgs.pkgs.writeShellScriptBin "start" ''
    ${pkgs.waybar}/bin/waybar &

    ${pkgs.swww}/bin/swww-daemon &

    ${pkgs.swww}/bin/swww img ~/.background-image &
  '';
in
{
  imports = [
    ./waybar.nix
  ];
  home.file.".background-image".source = ./desktop-wallpaper.jpg;

  wayland.windowManager.hyprland = {
    enable = true;
    settings = {
      exec-once = ''${startupScript}/bin/start'';

      decoration = {
        shadow_offset = "0 5";
        rounding = "10";
      };

      animations = {
        enabled = true;
      };

      "$mainMod" = "SUPER";
      "$terminal" = "kitty";
      "$browser" = "firefox";

      bind = [
        "$mainMod, Q, exec, $terminal"
        "$mainMod, F, exec, $browser"
        "$mainMod, C, killactive"
        "$mainMod, M, exit"
        "$mainMod, S, exec, rofi -show drun --show-icons"
        "$mainMod, h, movefocus, l"
        "$mainMod, l, movefocus, r"
        "$mainMod, j, movefocus, d"
        "$mainMod, k, movefocus, u"
      ];
    };
  };
}
