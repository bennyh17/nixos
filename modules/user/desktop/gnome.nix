{ username, ... }:

{
  home.file.".background-image".source = ./desktop-wallpaper.jpg;

  dconf.settings = {
    "org/gnome/desktop/background" = {
      "picture-uri" = "/home/${username}/.background-image";
    };
    "org/gnome/desktop/screensaver" = {
      "picture-uri" = "/home/${username}/.background-image";
    };

    # Enable minimize, maximize and close on windows
    "org/gnome/desktop/wm/preferences".button-layout = "minimize,maximize,close";
  };
}
