{ username, desktop, ... }:

{
  imports = [
    ./desktop/${desktop}.nix
    ./editor/nixvim/default.nix
    ./fastfetch/fastfetch.nix
    ./terminals/kitty.nix
    ./shells/zsh.nix
    ./multiplexer/tmux.nix
  ];

  # Declare user
  home = {
    username = username;
    homeDirectory = "/home/"+username;
  };

  programs.home-manager.enable = true; # Let Home Manager install and manage itself.

  home.stateVersion = "23.11";
}
