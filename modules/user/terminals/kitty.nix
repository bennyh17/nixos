{ config, ... }:

{
  # Kitty terminal for desktop installations
  programs = {
    kitty = {
      enable = true;
      shellIntegration.enableZshIntegration = true;
      theme = "Argonaut";
      extraConfig = ''
        background_opacity 0.8
      '';
      font = {
        size = 14.0;
        name = "Terminus";
      };
    };
  };
}
