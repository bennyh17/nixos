{ ... }:

{
  programs.fastfetch.enable = true;

  home.file.fastFetchLogo = {
    enable = true;
    source = ./logo/default;
    target = ".config/fastfetch/logo";
  };

  home.file.fastFetchConfig = {
    enable = true;
    source = ./config.jsonc;
    target = ".config/fastfetch/config.jsonc";
  };
}
