{ ... }:

{
  programs = {
    zsh = {
      enable = true;
      loginExtra = "fastfetch";
      autosuggestion.enable = true;
      sessionVariables = {
        TERM = "xterm-256color";
        EDITOR = "vi";
      };
      shellAliases = {
	v = "vi";
	".." = "cd ..";
	"..2" = "cd ../..";
	"..3" = "cd ../../..";
	"..4" = "cd ../../../..";
      };
      oh-my-zsh = {
	enable = true;
	theme = "fino-time";
	plugins = [ "kubectl" "tmux" "terraform" ];
      };
    };
    fzf = {
      enable = true;
      enableZshIntegration = true;
    };
    lsd = {
      enable = true;
      enableAliases = true;
    };
    zoxide = {
      enable = true;
      enableZshIntegration = true;
    };
  };
}
