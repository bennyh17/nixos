{ pkgs, ... }:

{
  programs.nixvim = {
    plugins = {
      alpha = {
        enable = true;
        theme = "startify";
      };
      lualine.enable = true;
      chadtree.enable = true;
      telescope.enable = true;
      treesitter.enable = true;
      barbar.enable = true;
      gitgutter.enable = true;
      undotree.enable = true;
      comment.enable = true;
      lsp = {
        enable = true;
        servers = {
          nixd.enable = true;
          pylsp.enable = true;
	};
      };
    };
    extraPlugins = with pkgs.vimPlugins; [
      lazygit-nvim
      vim-ledger
      vim-table-mode
    ];
  };
}
