{ ... }:

{
  imports = [
    ./plugins.nix
    ./keymaps.nix
  ];

  programs = {
    ripgrep.enable = true;
    lazygit.enable = true;
  };

  programs.nixvim = {
    enable = true;
    defaultEditor = true;
    viAlias = true;
    vimAlias = true;
    colorschemes.catppuccin.enable = true;
    globals.mapleader = " ";
    opts = {
      number = true;
      relativenumber = true;
      shiftwidth = 2;
      termguicolors = true;
      undofile = true;
      tabstop = 4;
      expandtab = true;
    };
  };
}
