{ pkgs, kube_initial_master_ip, ... }:

{
  imports = [ 
    ../../modules/system/base-config.nix
    ../../modules/system/kubernetes/k3s.nix
  ];

  services.k3s = {
    role = "server"; # Set k3s to server mode
    serverAddr = "https://${kube_initial_master_ip}:6443"; # Set the initial servers IP
  };

  environment.systemPackages = with pkgs; [
    kubernetes-helm # Install helm
  ];
}
