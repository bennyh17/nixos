{
  pkgs,
  username,
  slave_interface,
  bridge_ip,
  bridge_prefix,
  bridge_gateway,
  hypervisor_domain,
  ...
}:

{
  imports = [
    ../../modules/system/base-config.nix
  ];

  users.users.${username}.extraGroups = [ "libvirtd" ]; # Add user to libvirtd group

  virtualisation.libvirtd = {
    enable = true;
    onBoot = "start";
    onShutdown = "shutdown";
  };

  # Need to set URI here since it defaults to qemu:///user
  environment.sessionVariables = {
    LIBVIRT_DEFAULT_URI = "qemu:///system";
  };

  # Custom packages
  environment.systemPackages = with pkgs; [
    libxslt
    terraform
    virt-manager
  ];
  
  # Create a bridge out by enslaving physical interface
  networking = {
    nameservers = [ "${bridge_gateway}" ];
    domain = "${hypervisor_domain}";
    interfaces.br0.ipv4 = {
      addresses = [
        {
          address = "${bridge_ip}";
          prefixLength = bridge_prefix;
        }
      ];
      routes = [
        {
          address = "0.0.0.0";
          prefixLength = 0;
          via = "${bridge_gateway}";
        }
      ];
    };
    bridges = {
      "br0" = {
        interfaces = ["${slave_interface}"];
      };
    };
  };
}
