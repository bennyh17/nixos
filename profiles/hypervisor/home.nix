{ lib, ... }:

{
  imports = [ 
    ../../modules/user/base-config.nix 
  ];

  home.file.fastFetchLogo.source = lib.mkOverride 50 ../../modules/user/fastfetch/logo/hypervisor;
}
