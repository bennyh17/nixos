{ kube_initial_master_ip, ... }:

{
  imports = [ 
    ../../modules/system/base-config.nix
    ../../modules/system/kubernetes/k3s.nix
  ];

  services.k3s = {
    role = "agent"; # Set k3s to agent mode
    serverAddr = "https://${kube_initial_master_ip}:6443"; # Set the initial servers IP
  };
}
