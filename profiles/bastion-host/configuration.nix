{ pkgs, ... }:

{
  imports = [
    ../../modules/system/base-config.nix
    ../../modules/system/security/sshd-bastion.nix
  ];

  environment.systemPackages = with pkgs; [ 
    nmap
  ];
}
