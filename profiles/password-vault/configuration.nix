{ config, pkgs, ... }:

{
  imports = [
    ../../modules/system/base-config.nix
  ];

  environment.systemPackages = with pkgs; [
    ledger
    ledger-autosync
    pass
  ];
}
