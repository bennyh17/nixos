{ lib, ... }:

{
  imports = [
    ../../modules/user/base-config.nix
    ../../modules/user/helm/default.nix
  ];

  home.file.fastFetchLogo.source = lib.mkOverride 50 ../../modules/user/fastfetch/logo/k3s-master;
}
