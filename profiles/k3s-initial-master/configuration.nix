{ pkgs, ... }:

{
  imports = [ 
    ../../modules/system/base-config.nix
    ../../modules/system/kubernetes/k3s.nix
  ];

  services.k3s = {
    clusterInit = true; # Initialize cluster
    role = "server"; # Set k3s to server mode
  };

  # Include helm package manager
  environment.systemPackages = with pkgs; [
    k9s
    kubernetes-helm
  ];
}
